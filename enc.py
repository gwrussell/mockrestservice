import base64
import sys


enc = base64.b64encode(sys.argv[1].encode('ascii'))
print(enc.decode('UTF-8'))
