var _ = require('lodash');
var fs = require('fs');
// TODO -- maybe add defaults
var RequiredValues = [
    {"name":"globalResponseDelaySeconds", "regex":"[\\d]+"},
    {"name":"secret", "regex":"[\\s\\S]+"},
    {"name":"validPasswordRegex", "regex":"[\\s\\S]+"},
    {"name":"validUserRegex", "regex":"[\\s\\S]+"},
    {"name":"jwtExpiresSeconds", "regex":"[\\d]+"}
];


var config = {};
exports.CONFIG_FILE = "data/config.json";

exports.loadConfig = function() {
    try {


        var contents = fs.readFileSync(exports.CONFIG_FILE);
        config = JSON.parse(contents);
        validate(config);
        console.log("Loaded config");

    } catch(err) {
        console.log(err.message);
        return;
    }
}

exports.get = function(key) {
    if(typeof config[key] !== "undefined") {
        return config[key];
    }
    else {
        console.log("Trying to get nonexistent config value "+key);
        return null;
    }
}

exports.getConfig = function() {
    return config;
}

exports.add = function(key, val) {
    config[key]=val;
    save();
}

exports.replace = function(obj) {
    config = obj;
    save();
}

function save() {
    fs.writeFile(exports.CONFIG_FILE, JSON.stringify(config), function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("Config updated");
    });
}

function validate(config) {
    isValid = true;
    for(var idx=0; idx < RequiredValues.length; idx++) {
        if(config.hasOwnProperty(RequiredValues[idx].name)) {
            var regex = new RegExp(RequiredValues[idx].regex)
                if(!regex.test(config[RequiredValues[idx].name])) {
                    console.error("Config value "+RequiredValues[idx].name+" is invalid");
                    isValid=false;
                }
        } else {
            console.error("Required config value "+ RequiredValues[idx].name + " not in config.");
            isValid = false;
        }
    }

    return isValid;
}




