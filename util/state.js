var state = {};

exports.add = function(key, val) {
    state[key] = val;
}

exports.get = function(key) {
    return state[key];
}