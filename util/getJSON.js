var fs = require("fs");
var path = require("path");
var _ =require("lodash");

exports.getJson = function(url) {

    try {
        var parsed = path.parse(url);

        var filePath = buildJsonPath(parsed);
        console.log("Looking for json file: " + filePath);
        var contents = fs.readFileSync(filePath);
        var jsonContent = JSON.parse(contents);
    } catch(err) {
        console.log(err.message);
        return;
    }

    return jsonContent;
}

function buildJsonPath(parsed) {
    var filePath = "data/api" + parsed["dir"];
    if(!_.endsWith(filePath, '/')) {
        filePath+='/';
    }
    filePath += parsed["base"] + ".json";
    return filePath;

}