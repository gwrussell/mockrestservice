var request = require('request');
var url = require('url');
var _ = require('lodash');

exports.callback = function(options, req) {
    try {
        var body = req.body;
        var parsedUri = parseCallBackUri(options, req);
        var i = 0;
        var cbOptions = {
            "url": parsedUri["href"],
            "method": options.method,
            "headers": options.headers,
            "form": options.body
        };
        console.log("Calling back to : "+cbOptions.url);
        request(cbOptions, function(err, resp, body){
            if(!_.isUndefined(resp) &&  resp.statusCode === 200) {
                console.log("Callback success");
                console.log(body);
            }
            else {
                console.log("Callback failed.");
                console.log(err);
            }
        })
    } catch(err) {
        console.log("Error with callback: "+err);
    }


}

function parseCallBackUri(options, req) {
    if (_.isEmpty(options.uri)) {
        throw "Uri object is empty";
    }
    if(!_.isEmpty(options.uri.requestCallBackName)) {
        var cbUri = req.body[options.uri.requestCallBackName];
    }
    if(_.isEmpty(cbUri)) {
        // Check for a default callback uri
        cbUri = options.uri.callBackUri;
    }
    if(_.isEmpty(cbUri)) {
        // All out of ideas
        throw "Cannot parse callback uri";
    }
    return url.parse(cbUri);

}