var getj = require("./getJSON.js");
var _ = require("lodash");
var cb = require('./callback');
var config = require('./config');

exports.handle = function(req, res, next) {
    // Get the json corresponding to this endpoint
    var json=getj.getJson(req.url);
    if(_.isEmpty(json)) {
        returnBad(res, 404);
        return;
    }
    var verbJson = getJsonForVerb(json, req.method.toUpperCase());
    if(_.isEmpty(verbJson)) {
        returnBad(res, 405);
        return;
    }
    appendHeaders(res, verbJson.headers);
    // All good

    // Launch callback, if any
    if (!_.isEmpty(verbJson.callback)) {
        delaySeconds = _.isNumber(verbJson.callback.delaySeconds) ? verbJson.callback.delaySeconds : 1;
        delayMillis = delaySeconds * 1000;
        setTimeout(cb.callback, delayMillis, verbJson.callback, req);
    }

    // Return after a delay
    var responseDelay = config.getConfig()["globalResponseDelaySeconds"];
    if (!_.isNumber(responseDelay)) {
        responseDelay = 1000;
    }
    setTimeout(returnGood, responseDelay * 1000, res, verbJson);

}

function returnGood(res, verbJson) {
    console.log("Returning success");
    try {
        res.statusCode = verbJson.response.status;
        res.json(verbJson.response.body);
    } catch(err) {
        console.error(err);
    }


}

function returnBad(res, code) {
    res.statusCode = code;
    res.send();
}
function appendHeaders(res, headers) {
    for (var idx in headers) {
        if(headers.hasOwnProperty(idx)) {
            res.append(idx, headers[idx]);
        }
    }
}
// Get the json corresponding to the request method.
// Method can be an array or string in the JSON.
function getJsonForVerb(jsonObj, reqMethod) {
    for (var idx = 0; idx < jsonObj.length; idx++) {
        var jsonMethods = jsonObj[idx].request.method;
        if (_.isArray(jsonMethods)) {
           for (var jdx=0; jdx < jsonMethods.length; jdx++ ) {
               if (jsonMethods[jdx].toUpperCase() === reqMethod) {
                   return {
                       "response": jsonObj[idx].response,
                       "callback": jsonObj[idx].callback
                   }

               }
           }
        } else {
            // jsonMethods is a string.  If not, we've got bigger problems...
            if (jsonMethods.toUpperCase() === reqMethod) {
                return {
                    "response": jsonObj[idx].response,
                    "callback": jsonObj[idx].callback
                }
            }
        }

    }

}