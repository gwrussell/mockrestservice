var jwt = require('jsonwebtoken');
var config = require('./config');
var endpoint = /auth|token/;

function authenticate(userName, passWord)
{
    var pwRegex = new RegExp(config.get("validPasswordRegex"));
    var userRegex = new RegExp(config.get("validUserRegex"));
    if (!pwRegex.test(passWord)) {
        console.log("Password does not match regex " + userRegex.toString());
        return false;
    }
    if (!userRegex.test(userName)) {
        console.log("Username does not match regex " + pwRegex.toString());
        return false;
    }

    return true;

}
exports.handleRouteAuth = function(req, res, next) {
    var useAuth = config.get("useAuth");
    if(useAuth) {
        var authHeader = req.get("Authorization");
        if (!authHeader) {
            unauthorized("Missing Authorization header", res);
            return;
        }

        var headerSplit = authHeader.split(" ");
        if (headerSplit.length !== 2 || headerSplit[0].toUpperCase() !== "BEARER") {
            unauthorized("Invalid Authorization Header", res);
            return;
        }
        var token = headerSplit[1];

        if(!token) {
            unauthorized("No token provided.", res);
            return;
        }
        jwt.verify(token, config.get("secret"), function(err, decoded) {
            if (err) {
                unauthorized("Unable to authenticate token: " + err.message, res);
                return;
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });


    } else {
        next();
    }



}
exports.setRoutes = function(app) {
    app.get(endpoint, doGet);

    app.post(endpoint, function (req, res, next) {
        try {
            Credentials = getCredentialsFromHeader(req);
        }
        catch(err) {
            res.statusCode = 400;
            res.send();
        }

        if(authenticate(Credentials.user, Credentials.pass)) {
            var token = jwt.sign
            (
                {
                    "roles":["all", "the", "roles"]
                },
                config.get("secret"),
                {
                    expiresIn: config.get("jwtExpiresSeconds")
                }
            );
            res.json({

                "access_token": token,
                "token_type": "bearer",
                "expires_in": config.get("jwtExpiresSeconds")
            });
        } else {
            res.statusCode = 401;
            res.send();
        }
    });
}

function unauthorized(message, res) {
    res.status(401).json({
        message: message
    });
}
function getCredentialsFromHeader(req) {
    var i = 1;
    var header = req.header("Authorization");
    if(!header) {
        throw "No Auth header supplied."
    }
    var headerSplit = header.split(" ");
    if (!headerSplit[0] || headerSplit[0].toUpperCase() !== 'BASIC') {
        throw "Invalid Auth type";
    }
    var buf = new Buffer(headerSplit[1], 'base64');
    var credentials = buf.toString().split(':');
    if (credentials.length !== 2) {
        throw "Invalid credential encoding";
    }
    return {
        user: credentials[0],
        pass: credentials[1]
    };


}
function doGet(req, res, next)
{

    res.statusCode = 200;
    res.json({
        "message": "Hello from the Auth API. Use POST with Basic Auth if you want a token."
    });
    res.send();
}


