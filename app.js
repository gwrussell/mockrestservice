var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var urlParser = require('./util/URLParser');
var handler = require('./util/jsonHandler');
var routes = require('./routes/index');
var users = require('./routes/users');
var chokidar = require('chokidar');
var config = require('./util/config');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var auth = require("./util/auth");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(morgan('dev'));

// Set up auth routes
auth.setRoutes(app);

var apiRoutes = [auth.handleRouteAuth, handler.handle];

app.use("/api", apiRoutes);



app.use("/admin", function(req, res) {
  var i =0;
  var method = req.method.toUpperCase();
  // TODO --allow updating instead of just replacing
  if (method === 'POST' || method === 'PUT') {
    config.replace(req.body);
    // TODO --handle errors nicely
    res.statusCode = 200;
    res.json(config.getConfig());
    res.send();

  } else if (method === 'GET') {
    res.statusCode = 200;
    res.json(config.getConfig());
    res.send();
  }


});

var watcher = chokidar.watch(config.CONFIG_FILE, {
  "persistent": true
});
config.loadConfig();
watcher.on('change', config.loadConfig);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
